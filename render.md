# Rendering

- [Physically Based Shading in Theory and Practice](http://blog.selfshadow.com/publications/) (2012-2015) (SIGGRAPH courses)
- [Advances in Real-Time Rendering in Games](http://advances.realtimerendering.com/) (2012-2015) (SIGGRAPH courses, great source for general content, see 2014 and 2015)
- [Adopting a physically based shading model](https://seblagarde.wordpress.com/2011/08/17/hello-world/) (2011-2015)
- [Specular BRDF Reference](http://graphicrants.blogspot.ca/2013/08/specular-brdf-reference.html) (2013) (Karis @ Epic Games)
- [A sampling of shadow techniques](https://mynameismjp.wordpress.com/2013/09/10/shadow-maps/) (2013-2015) (also his whole blog)

